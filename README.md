# Explore Engagement Law
*Elodie Peyrol-Kleiber, MIMMOC, Université de Poitiers, 2023*

*Michael Nauge, DRInnov, Université de Poitiers, 2023*


## Contexte
"I wanted to find a way to gather and analyze my data while also creating a long-lasting tool for colleagues and students. Indeed, the legislation about indentured servitude has never been the object of a thorough study across colonies and empires. So I wanted a tool that could serve me as well as others, research and pedagogy."

*Elodie Peyrol-Kleiber*

## Objectif
Prototyper un outil d'exploitation de la base de donnée de lois sur l'engagement colonial français anglais publié sur l'entrepôt Nakala.

*Peyrol-Kleiber, Elodie (2023) «Base de données de lois sur l'engagement colonial français anglais 17-18 ième siècles» [Dataset] NAKALA. https://doi.org/10.34847/nkl.cafb7m71*


## Utilisation


### Execution en ligne
> Pour une exécution *ponctuelle*
[![Binder](https://mybinder.org/badge_logo.svg)](https://mybinder.org/v2/git/https%3A%2F%2Fgitlab.huma-num.fr%2Fmshs-poitiers%2Fmimmoc%2Fexploreengagementlaw/HEAD)

### Execution locale
> Pour une utilisation *régulière* ou si l'application distante MyBinder rencontre un problème,
il est possible de lancer les scripts sur son ordinateur personnel.

Pour cela :
1. Téléchargez sur votre bureau et décompresser l'[archive](https://gitlab.huma-num.fr/mshs-poitiers/mimmoc/exploreengagementlaw/-/archive/main/exploreengagementlaw-main.zip)
1. Installez l'environnement de datascientist [Anaconda](https://www.anaconda.com/products/distribution)
1. Lancez le logiciel Jupyter (inclus dans Anaconda), ce qui lancera également votre navigateur internet
1. Depuis Jupyter, naviguez dans vos dossiers pour ouvrir l'archive téléchargée et ouvrir le fichier **exploreEngagementLaw.ipynb**

