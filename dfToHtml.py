# -*- coding: utf-8 -*-
"""
Created on Thu May 28 15:57:29 2020

@author: mnauge
"""


import re



def df2htmlNav(df, pathHtmOut):
    with open(pathHtmOut, 'w', encoding="utf-8",newline='\n') as htm:
        
        htm.write('<ul>')
        
        for index, row in df.iterrows():
            
            idLine = str(row['id'])

            htm.write('<li><a href="#'+idLine+'">'+idLine+'</a></li>\n')
        
        htm.write('</ul>')
        

def df2htmlBody(df, paramsFilt, pathHtmOut):
    
    #print(df.columns)
    with open(pathHtmOut, 'w', encoding="utf-8",newline='\n') as htm:
        htm.write('<h1>Corpus Sample</h2>\n')

        #ecriture des params filters
        htm.write('<h2>Used Filters</h2>\n')
        htm.write('<p class="paramsFilt">')
        for k in paramsFilt:
            htm.write('<b>'+k+'</b> : '+str(paramsFilt[k])+" ")
        htm.write('</p>')

       
        htm.write('<h2 id="entries">Entries</h2>\n')
        for index, row in df.iterrows():
            
            idLine = str(row['id'])

            strTitle = ""
            
            htm.write('<p id="'+idLine+'">')
            
            year = str(row['yearR'])
            if len(year)>0:
                strTitle+='<b>'+year+'</b>'
        
            colonieName = row['colony_en']

            strTitle+="\n"
            if len(colonieName)>0:
                strTitle+= " "+ colonieName
                
            strTitle+="\n"
            htm.write(strTitle)
            
            sujet = row['sujet 1_en']
            if len(sujet)>0:
                htm.write('<b> #'+sujet+' </b>') 
                
            sujet = row['sujet 2_en']
            if len(sujet)>0:
                htm.write('<b> #'+sujet+' </b>')
                
            sujet = row['sujet 3_en']
            if len(sujet)>0:
                htm.write('<b> #'+sujet+' </b>') 
                

            theme = row['thème_en']
            if len(theme)>0:
                htm.write('<i>'+theme+'</i> \n')
                
            htm.write("\n")


            
            source = row['source']
            if len(source)>0:
                htm.write("from :"+source+' \n')


            urlSource = str(row["urlTxt"])
            
            #if len(urlSource)>0:
            if "http" in urlSource:

                htm.write('<a href="'+urlSource+'" >Source</a>')
                
                htm.write('<iframe width="800" height="600"src="'+urlSource+'"></iframe>')
                
            else:
                htm.write('<i>'+urlSource+'</i> \n')

            


            htm.write('</p>') 
            htm.write('\n')
            
            
def mergePartsHtml(pathHtmlOut, pathHtmNav, pathHtmBody):
    
    pathpart1 = "./htmlPattern/head.html"
    pathpart2 = pathHtmNav
    pathpart3 = "./htmlPattern/bodyStart.html"

    pathpart5 = pathHtmBody
    pathpart6 = "./htmlPattern/bodyStop.html"

    pathParts = [pathpart1, pathpart2, pathpart3, pathpart5,pathpart6]
    with open(pathHtmlOut, "w",encoding="utf-8",newline='\n') as htm:
        
        for pathPart in pathParts:
            with open(pathPart,encoding="utf-8") as partCur:
                read_data = partCur.read()
                htm.write(read_data)


def buildHtmlFromDf(df, paramsFilt, pathDirOut, filenameHtmOut):
    
    pathHtmOut = pathDirOut+filenameHtmOut
    
    pathHtmlNav =  pathDirOut+"tmp_nav.html"
    df2htmlNav(df, pathHtmlNav)
    
    pathHtmlBody =  pathDirOut+"tmp_body.html"
    df2htmlBody(df, paramsFilt,pathHtmlBody)
    
    mergePartsHtml(pathHtmOut, pathHtmlNav, pathHtmlBody)
    
    return True





